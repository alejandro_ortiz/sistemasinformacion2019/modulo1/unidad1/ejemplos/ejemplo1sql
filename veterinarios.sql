﻿-- Ejemplo de creacion de base de datos de veterinarios
-- Tiene 3 tablas

DROP DATABASE IF EXISTS b20190528;
CREATE DATABASE b20190528;
USE b20190528;

/*
  creando la tabla animales

*/

CREATE TABLE animales(
    id int AUTO_INCREMENT,
    nombre varchar(100),
    raza varchar(100),
    fechaNac date,
    PRIMARY KEY (id)
  );

CREATE TABLE veterinarios(
    codigo int AUTO_INCREMENT,
    nombre varchar(100),
    especialidad varchar(100),
    PRIMARY KEY(codigo) -- creando la clave principal
  );


CREATE TABLE acuden(
    idAnimal int,
    codVet int,
    fecha date,
    PRIMARY KEY (idAnimal, codVet, fecha),
    UNIQUE KEY (idAnimal),
    CONSTRAINT fkAcudenAnimales FOREIGN KEY (idAnimal) REFERENCES animales(id),
    CONSTRAINT fkAcudenVeterinarios FOREIGN KEY (codVet) REFERENCES veterinarios(codigo)
  );

-- insertar un registro
  
  INSERT INTO animales (nombre, raza, fechaNac) VALUES
  ('Jorge','bulldog','2000/2/1'),
  ('Ana','caniche','2002/1/5'); 

-- listar

  SELECT * FROM animales;